#!/bin/bash

echo "Que voulez vous faire ?"

echo "
    1. Installer docker sur des machines.
    2. Voir l'installation de docker sur des machines.
    3. Mettre en swarm des machines.
    4. Sortir des machines du swarm.
    5. Désinstaller Docker.
    "
    
echo  "Entrer un nombre entre 1,2,3,4 et 5"

read value

while [[ $value -lt 1 || $value -gt 5 ]]; do
      
      value=""
      echo "Votre choix doit etre dans l'intervalle 1-5"; 
      read value
done

echo " Vous aviez choisir $value"

case "$value" in

1)  echo "Installer docker sur des machines."
    echo "Vous voulez installer docker sur combien de machine"
    read nb_machine

    while [[ $nb_machine -lt 1 || $nb_machine -gt 3 ]]; do
      
      nb_machine=""
      echo "Le nombre de machine doit pas depasser trois"; 
      read nb_machine
    done

    counter=1
    until [ $counter -gt $nb_machine ]
    do
       echo "Entrer l'ip du Serveur $counter"
       read IP_machine
       echo "Entrer le user du Serveur $counter"
       read User
       echo "Vous aurai a entrer le mot de pas de connection ssh"
       read Password       
       sleep 5s
       sshpass -p $Password ssh $User@$IP_machine 'bash -s' < install-docker.sh

       ((counter++))
    done
    ;;
2)  echo  "Voir l'installation de docker sur des machines."
    echo "Vous voulez voir l'installation de docker sur combien de machine"
    read nb_machine

    while [[ $nb_machine -lt 1 || $nb_machine -gt 3 ]]; do
      
      nb_machine=""
      echo "Le nombre de machine doit pas depasser trois"; 
      read nb_machine
    done

    counter=1
    until [ $counter -gt $nb_machine ]
    do
       echo "Entrer l'ip du Serveur $counter"
       read IP_machine
       echo "Entrer le user du Serveur $counter"
       read User
       echo "Vous aurai a entrer le mot de pas de connection ssh"
       read Password       
       sleep 5s
       sshpass -p $Password ssh $User@$IP_machine 'bash -s' < check-docker-version.sh

       ((counter++))
    done
    ;;
3)  echo  "Mettre en swarm des machines."
    echo "Vous voulez mettre en swarm combien de machine"
    read nb_machine

    while [[ $nb_machine -lt 1 || $nb_machine -gt 3 ]]; do
      
      nb_machine=""
      echo "Le nombre de machine doit pas depasser trois"; 
      read nb_machine
    done

    counter=1
    until [ $counter -gt $nb_machine ]
    do
        if [ $counter == 1 ]; then
            echo "Entrer l'ip du Manager "
            read MANAGER_ADDRESS
            echo "Entrer le user du Manager "
            read User
            echo "Vous aurai a entrer le mot de pas de connection ssh"
            read Password       
            sleep 3s
            sshpass -p $Password ssh $User@$MANAGER_ADDRESS  "docker swarm leave --force"
            sshpass -p $Password ssh $User@$MANAGER_ADDRESS  "docker swarm init --advertise-addr $MANAGER_ADDRESS"
            MANAGER_TOKEN=$(sshpass -p $Password ssh $User@$MANAGER_ADDRESS  "docker swarm join-token manager -q")
            WORKER_TOKEN=$(sshpass -p $Password ssh $User@$MANAGER_ADDRESS  "docker swarm join-token worker -q")


        else 
            echo "Entrer l'ip du Worker $counter"
            read WORKER_ADDRESS
            echo "Entrer le user du Worker $counter"
            read User
            echo "Vous aurai a entrer le mot de pas de connection ssh"
            read Password
            sleep 3s
            sshpass -p $Password ssh $User@$WORKER_ADDRESS  "docker swarm leave --force"
            sshpass -p $Password ssh $User@$WORKER_ADDRESS  "docker swarm join --token $WORKER_TOKEN   $MANAGER_ADDRESS" 
        fi    
       ((counter++))
    done
    sshpass -p $Password ssh $User@$MANAGER_ADDRESS  "docker node ls" 

    ;;
4) echo  "Sortir des machines du swarm."
   echo "Entrer l'ip du Manager du swarm"
   read IP_MANAGER_SWARM
   echo "Entrer le user du Serveur."
   read User
   echo "Vous aurai a entrer le mot de pas de connection ssh"
   read Password       
   sleep 2s
   echo "Voici les machines du swarm"
   MACHINE_IN_SWARM=$(sshpass -p $Password ssh $User@$IP_MANAGER_SWARM 'bash -s' < liste_machine_in_swarm.sh)
   echo $MACHINE_IN_SWARM
   echo "Vous voulez enlever  combien de machine du swarm"
   read nb_machine

    while [[ $nb_machine -lt 1 || $nb_machine -gt 3 ]]; do
      
      nb_machine=""
      echo "Le nombre de machine doit pas depasser trois"; 
      read nb_machine
    done

    counter=1
    until [ $counter -gt $nb_machine ]
    do
        echo "Entrer l'ip du Serveur $counter"
        read IP_machine
        while [ "$IP_machine" == "$IP_MANAGER_SWARM" ];
        do
            IP_machine=""
            echo "Entrer l'ip du Serveur $counter"
            read IP_machine
        done
        echo "Entrer le user du Serveur $counter"
        read User
        echo "Vous aurai a entrer le mot de pas de connection ssh"
        read Password
        sleep 5s
        sshpass -p $Password ssh $User@$IP_machine "docker swarm leave --force"
        ((counter++))
    done
   ;;
5) echo  "Désinstaller Docker."
   echo "Vous voulez désinstaller docker sur combien de machine"
    read nb_machine

    while [[ $nb_machine -lt 1 || $nb_machine -gt 3 ]]; do
      
      nb_machine=""
      echo "Le nombre de machine doit pas depasser tois"; 
      read nb_machine
    done

    counter=1
    until [ $counter -gt $nb_machine ]
    do
       echo "Entrer l'ip du Serveur $counter"
       read IP_machine
       echo "Entrer le user du Serveur $counter"
       read User
       echo "Vous aurai a entrer le mot de pas de connection ssh"
       read Password       
       sleep 5s
       sshpass -p $Password ssh $User@$IP_machine 'bash -s' < uninstall-docker.sh

       ((counter++))
    done
   ;;   
esac
