#!/bin/bash
#to check package is installed or not without distribution dependency
#!/bin/bash
#Part for docker installing
set -eu

# Docker

echo "uninstall docker"

sudo apt-get purge -y docker-engine docker docker.io docker-ce docker-ce-cli

sleep 1s

sudo apt-get autoremove -y --purge docker-engine docker docker.io docker-ce

sleep 2s

echo "Docker is uninstall now"

docker ps
