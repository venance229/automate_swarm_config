#!/bin/bash
#to check package is installed or not without distribution dependency
#!/bin/bash
#Part for docker installing
set -eu

# Docker 

echo "Check if docker and docker-compose are install and remove"

#sudo apt-get remove --yes docker docker-engine docker.io containerd runc

#sudo apt remove --yes docker docker-engine docker.io

#sudo apt-get purge docker-ce docker-ce-cli containerd.io

sudo apt-get purge docker-ce -y

echo "Docker and Docker-compose install"

apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common sudo

curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | sudo apt-key add -

add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"

apt-get update

apt-get install docker-ce

curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose

docker-compose --version


curl -fsSL https://get.docker.com/ -o get-docker.sh
sh get-docker.sh

echo "Docker swarm configuration on master node"

#Docker swarm configuration on master node

docker swarm leave --force

docker swarm init --advertise-addr 10.10.25.165

#Get swarm manager INFO

#MANAGER_ADDRESS=$(docker swarm join-token worker | tail -n 2 |  tr -d '[[:space:]]')

MANAGER_ADDRESS=10.10.25.165

#echo "$MANAGER_ADDRESS"

MANAGER_TOKEN=$(docker swarm join-token manager -q)

#echo "$MANAGER_TOKEN"

WORKER_TOKEN=$(docker swarm join-token worker -q)

#echo "$WORKER_TOKEN"

echo "Node add step"

touch add_node.sh

chmod +x add_node.sh

echo "docker swarm leave --force" > add_node.sh

echo "docker swarm join --token $WORKER_TOKEN   $MANAGER_ADDRESS" >> add_node.sh

cat add_node.sh

echo "At this step you need to give root password for your node worker"

#change 10.10.25.166 by your worker ip adresse

ssh root@10.10.25.166 'bash -s' < add_node.sh 

docker node ls

