#!/bin/bash
# Execute: ./createswarm.sh ip_manager ip_node user_node
set -eu
#Docker swarm configuration on master node

docker swarm init --advertise-addr $1

MANAGER_TOKEN=$(docker swarm join-token manager -q)
WORKER_TOKEN=$(docker swarm join-token worker -q)

sleep 3s

docker node ls
