#!/bin/bash
#to check package is installed or not without distribution dependency
#!/bin/bash
#Part for docker installing
set -eu

echo "Remove docker swarm configuration on master node"

#Remove docker swarm configuration on master node

WORKER_ID=$(docker node ls -f name=serveur18 |  awk  '{print $1}'  | tail -n1)

docker node demote $WORKER_ID

docker swarm leave --force

echo "Remove node  step"

touch remove_node.sh

chmod +x remove_node.sh

echo "docker swarm leave --force" > remove_node.sh

cat remove_node.sh

echo "At this step you need to give root password for your node worker"

ssh root@10.10.25.166 'bash -s' < remove_node.sh


